define(['app', 'lib'], function() {
  app.config = {
    panel: true,
    panelAutoOpen: true
  };
  app.homeUrl = {
    cls: 'home'
  };
  app.debug = true;
  app.disableCache = true;
  lib.dateType = 'string';
  lib.number.ds = '.';
  lib.number.dp = ',';
  lib.autoMask = true;
  lib.i18n.debug = true;
  lib.logAjax = false;
  lib.flags = true;
  lib.DEBUG_MESSAGES = true;
  return app.config;
});