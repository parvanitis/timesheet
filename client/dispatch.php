<?php

  
$libPath=getenv('unity_lib_path') or $path=$_SERVER['unity_lib_path'];
$libUrl=getenv('unity_lib_url') or $path=$_SERVER['unity_lib_url'];
if (!$libPath or !$libUrl) die("lib paths not set in environment. set in apache conf, htaccess, environment or manually");

require_once $libPath . '/libc/unity/runtimes/startup.php';
require_once '../config.php';
$config=new MYCONFIG();
$config->run();

function blockBrowse ($event, $params){        
      if($event == 'beforeDispatch'){
          if($params['_run']){
             $runParam = strtoupper($params['_run']);
             if(strpos($runParam,'STATEMENTDATA')===0 || strpos($runParam,'TRADEENTRY')===0){
                 if(!$params['xaction']){
                     throw new USER_EXCEPTION("Invalid call");
                 }
                 if(!($runParam==='STATEMENTDATA->CRUD_OPS' && strtoupper($params['xaction'])==='CREATE')){
                      throw new USER_EXCEPTION("Invalid call");
                 }                 
             }
          }
      }
  }

$dispatcher=new DISPATCHER();
$dispatcher->hook = 'blockBrowse';

  
  
$dispatcher->handle();

?>
