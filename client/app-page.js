define(['config', 'menu', 'app', 'app-controls', 'app-components-common', 'app-local'], function(config, menu) {
  var page = {
    xtype: 'page',
    className: 'ui-responsive-panel',
    events: {},
    items: [
      {
        id: 'pageHeader',
        xtype: 'header',
        position: 'fixed',
        items: [
          config.logo || app.stdButtons.get(config.panel ? 'layout:menu' : 'layout:home', {
            id: 'pageHeader-leftButton',
            className: 'unity-ctx-button',
            xtype: 'link'}),
          {id: 'main-title', tagName: 'h1'},
          {className: 'ui-btn-right ctx-menu-wrapper'},
          {className: 'ui-timer'}
        ]
      },
      {
        id: 'pageContent',
        xtype: 'content'
      },
      {
        id: 'pageFooter',
        xtype: 'footer',
        position: 'fixed',
        items: [
          {
            id: 'bottom-navbar',
            xtype: 'navbar',
            iconPos: 'left',
            items: [
              {className: 'unity-ctx-button', enabled: false, icon: 'none'},
              {className: 'unity-ctx-button', enabled: false, icon: 'none'},
              {className: 'unity-ctx-button', enabled: false, icon: 'none'}
            ]
          }
        ]
      },
      config.panel ?
              {
                id: 'main-leftPanel',
                xtype: 'panel',
                position: 'left',
                position_fixed: true,
                dismissible: false,
                display: 'reveal',
                items: [menu]
              } : null,
      {
        id: 'main-rightPanel',
        className: 'ui-panel ui-panel-fixed ui-panel-display-overlay ui-panel-position-right ui-panel-closed ui-body-c ui-panel-animate',
        css: {
          'z-index': 1,
          zoom:1
        }
      }
    ]
  };

  return page;
});
