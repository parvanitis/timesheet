define(function () {
    return {
        xtype: 'treemenu',
        items: [
            {
                group: true,
                items: [
                     {
                        text: 'New',
                        collapsed: false,
                        items: [
                            {text: 'Start new timesheet', href: {cls: 'appPath/timesheet/timesheet-start'}}
                        ]
                    },
                    {
                        text: 'Drafts',
                        collapsed: false,
                        items: [
                            {text: 'Show List', href: {cls: 'appPath/timesheet/timesheet-list'}}
                        ]
                    },
                    {
                        text: 'Projects',
                        collapsed: true,
                        items: [
                            {text: 'Show List', href: {cls: 'appPath/project/project-list'}},
                            {text: 'Add New', href: {cls: 'appPath/project/project'}}
                        ]
                    },
                    {
                        text: 'Clients',
                        collapsed: true,
                        items: [
                            {text: 'Show List', href: {cls: 'appPath/client/client-list'}}
                        ]
                    }
                ]
            }
        ]
    };
});