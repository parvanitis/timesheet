<?php
$libPath = getenv('unity_lib_path') or $path = $_SERVER['unity_lib_path'];
$libUrl = getenv('unity_lib_url') or $path = $_SERVER['unity_lib_url'];
if (!$libPath or !$libUrl)
    die("lib paths not set in environment. set in apache conf, htaccess, environment or manually");
require_once $libPath . '/libc/unity/runtimes/startup.php';
require_once '../config.php';
$config = new MYCONFIG(null, TRUE);
$config->runtimeUrl = $config->libUrl . '/libc/unity/runtimes/jqm';
$config->run();
?>
<!DOCTYPE html>
<html class="ui-mobile-rendering">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320"/>
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
        <meta http-equiv="cleartype" content="on"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
        <meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)" />

        <!--<link rel="stylesheet" href="css/app.css"/>-->

        <script type='text/javascript' src="<?= $config->libUrl ?>/libc/categorizr.js"></script>
        <script type='text/javascript' src="<?= $config->libUrl ?>/libc/unity/app-boot.js"></script>
        <script type='text/javascript' src="<?= $config->libUrl ?>/libc/require/require.js"></script>
        <script type='text/javascript' src="<?= $config->libUrl ?>/libc/unity/require-config.js"></script>

<!--<script src="js/jquery.countdown.js" type="text/javascript"></script>-->

        <!--[if lt IE 9]>
          <script src="css3-mediaqueries.js"></script>
        <![endif]-->

        <script type="text/javascript">

            require.config({
                paths: {
                    'app-page': 'app-page',
                    'app-layout': '<?= $config->runtimeUrl ?>/app-layout',
                    'index-css': '<?= $config->runtimeUrl ?>/index',
                    'appPath': 'app',
//                    'appCss': 'css/app',
                    'appComponents': 'components'
                }
            });

            require([
                'css!jquery-mobile',
                'css!mobiscroll-css',
                'css!unity/unity',
                'css!index-css',
//                'css!appCss',
                'jquery',
                'underscore',
                'backbone',
                'lib'
            ], function() {
                unity_boot.mobileInit();
                require(['app'], function() {
                    app.user =<?= php2js(APP::getUser(true)) ?>;
                    app.tenant =<?= php2js(APP::getTenant(true)) ?>;
                    require([
                        'app',
                        'config',
                        'app-content',
                        'app-dialogs',
                        'app-layout',
                        'app-page',
                        'app-local'
                    ], function() {
                        $(document).ready(_.bind(app.run, app));
                    });
                });
            });

        </script>

    </head>

    <body>
        <div data-role="page"></div>
    </body>

</html>