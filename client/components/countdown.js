/**
 * Countdown component
 * @returns start/end time as a lib.moment object
 */
define(['app-components'], function () {
    Ext.define('app.classes.countdown', {
        extend: 'app.classes.component',
        state: 'stopped',
        config: {},
        events: {
            'click #timer-startstop-btn': 'do_start',
            'click #timer-pause-btn': 'do_pause'
        },
        constructor: function () {
            this._interval = undefined;
            this.state = 'stopped';
            return app.classes.countdown.__super__.constructor.apply(this, arguments);
        },
        initialize: function () {
            this.options.items = this._buildItems();
            return app.classes.countdown.__super__.initialize.apply(this, arguments);
        },
        _setState: function (state) {
            this.state = state;
        },
        _getState: function () {
            return this.state;
        },
        afterRender: function () {
            this.timer = this.find('timer-container', 'itemId');
            this.actionbar = this.find('actionbar', 'itemId');
        },
        do_start: function (evt) {
            var state = this._getState();
            switch (state) {
                case 'started':
                    clearInterval(this._interval);
                    this._interval = undefined;
                    this._setState('stopped');
                    this.actionbar.find('startstop-btn').$el.text('Start');
                    this.actionbar.find('pause-btn').$el.text('Pause');
                    this.timeEnd = lib.moment(Date.now()).format('h:mm:ss');
                    lib.fire(app.content, 'timer:stop', {timeStart: this.timeStart, timeEnd: this.timeEnd});
                    this.timeStart = lib.moment(Date.now()).format('h:mm:ss');
                    this.clearValues();
                    break;
                case 'stopped':
                    this.clearValues();
                    this.startTimer();
                    this._setState('started');
                    this.actionbar.find('startstop-btn').$el.text('Stop');
                    lib.fire(app.content, 'timer:start');
                    break;
                case 'paused':
                    this.startTimer();
                    this._setState('started');
                    this.actionbar.find('startstop-btn').$el.text('Stop');
                    lib.fire(app.content, 'timer:paused');
                    break;
            }
        },
        do_pause: function (evt) {
            if ((this._getState() === 'stopped') || (this._getState() === 'paused'))
                return;
            clearInterval(this._interval);
            this._setState('paused');
            this.actionbar.find('startstop-btn').$el.text('Resume');
            lib.trigger(app, 'timer:pause');
        },
        startTimer: function () {
            this.timeStart = lib.moment(Date.now()).format('h:mm:ss');
            var th = this.timer.find('hour-item'),
                    tm = this.timer.find('minute-item'),
                    ts = this.timer.find('second-item');
            this._interval = setInterval(function () {
                var val = parseInt(ts.getValue()) + 1;
                ts.setValue(val);
                var d = lib.moment.duration(ts.getValue() * 1000, 'milliseconds');
                th.setValue(d.hours());
                tm.setValue(d.minutes());
            }, 1000);
        },
        clearValues: function () {
            this.timer.find('hour-item').setValue(0);
            this.timer.find('minute-item').setValue(0);
            this.timer.find('second-item').setValue(0);
        },
        openHandler: function (parent) {
            var o = this.options;
            if (o.handler) {
                app.loadContent({
                    cls: o.handler.cls,
                    params: o.handler.params,
                    parent: (parent) ? app.content : null
                });
            }
        },
        _buildItems: function () {
            var container = {
                className: 'container',
                items: []
            }, grid = {
                xtype: 'grid',
                columns: 2,
                items: []
            }, _timerContainer = {
                id: 'timer-container',
                itemId: 'timer-container',
                className: 'timer-container',
                items: []
            }, _actionContainer = this._createActionBarItem();
            _.each([
                {id: 'timer-hour', label: 'Hours', itemId: 'hour-item'},
                {id: 'timer-minute', label: 'Minutes', itemId: 'minute-item'},
                {id: 'timer-second', label: 'Seconds', itemId: 'second-item'}, ], function (item) {
                var im = this._createInputItem(item);
                _timerContainer.items.push(im);
            }, this);
            grid.items.push(_timerContainer);
            grid.items.push(_actionContainer);
            container.items.push(grid);
            return container.items;
        },
        _createInputItem: function (item) {
            return {
                className: 'timer-field',
                items: [
                    {
                        xtype: 'textfield',
                        className: item.id + ' timer-item',
                        id: item.id,
                        itemId: item.itemId,
                        readOnly:true,
                        value: 0
                    },
                    {
                        className: 'caption',
                        html: '<span>' + item.label + '</span>',
                        css: {
                            'font-size': '10px'
                        }
                    }
                ],
                css: {
                    width: '40px',
                    margin: '5px',
                    float: 'left',
                    'font-size': '20px'
                }
            };
        },
        _createActionBarItem: function () {
            return {
                xtype: 'buttonbar',
                className: 'action-bar',
                id: 'action-bar',
                itemId: 'actionbar',
                events: {
                    'click button': function (evt) {
                        var $this = $(evt.currentTarget);
                        lib.trigger(this, 'action:clicked', $this);
                    }
                },
                items: [
                    {
                        className: 'start timer-control',
                        label: 'Start',
                        id: 'timer-startstop-btn',
                        itemId: 'startstop-btn'
                    },
                    {
                        className: 'timer-control',
                        label: 'Pause',
                        id: 'timer-pause-btn',
                        itemId: 'pause-btn'
                    }
                ],
                getButton: function (itemId) {
                    return this.find(itemId, 'itemId');
                }
            };
        },
        destructor: function () {
            if (_.isNumber(this._interval))
                clearInterval(this._interval);
            app.classes.countdown.__super__.destructor.apply(this, arguments);
        }
    });
});