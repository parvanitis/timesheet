/*
 * this file contains everything that is related to the particular project runtime
 */

define(['unity/app-formhandler', 'app-layout', 'app-dialogs', 'lib', 'app', 'unity/app-behaviors',
  'unity/app-listhandler', 'unity/slickgrid', 'appComponents/countdown'], function() {

  if (lib.logAjax) {
    $(document).ajaxComplete(function(e, doj, obj) {
      var opts = JSON.parse(obj.data);
      var rstx = JSON.parse(doj.responseText);
      lib.log(opts._run, rstx.data);
    });
  }

  // @@@ App settings
  app.classes.date.prototype.options.format = 'DD/MM/YYYY';
  app.appdebug = true;
  app.user = 1;
  
  /**
   * Setup initial data before app starts
   */
  app.initPromise = $.Deferred();
  require(['appPath/user/user-schema'], function(userSchema) {
    var users = new userSchema.Users();
    users.fetch({
      params: {
        limit: 0 //all
      },
      success: function(resp) {
        app.users = resp;
        app.initPromise.resolve();
      },
      failure: function(err) {
        app.initPromise.reject();
      }
    });
  });

});