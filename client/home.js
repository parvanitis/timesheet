define([
  'home-cfg'
], function(fcfg) {
  var Handler = {
    initialize: function() {
      this.title = 'Home';
      this.cfg = fcfg.call(this);
      this.root = this.cfg.root;
      this.ready();
    }
  };
  return app.classes.ContentHandler.extend(Handler);
});