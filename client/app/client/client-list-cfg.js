define(function () {
    return function () {
        this.root = {
            items: [
                {
                    xtype: 'slickgrid',
                    id: 'client-grid',
                    collection: this.collection,
                    events: {},
                    options: {},
                    css: {},
                    columns: [
                        {id: 'SURNAME', name: 'Surname', field: 'SURNAME'},
                        {id: 'NAME', name: 'Name', field: 'NAME'},
                        {id: 'TIN', name: 'Tin', field: 'TIN'}
                    ]
                }
            ]
        };
    };
});