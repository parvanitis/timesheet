define(function() {
  'use strict';
  var Model = app.classes.model.extend({
    idAttribute: 'ID',
    mk_serverClass: 'CLIENT',
    mk_fields: {
      'ID': {type: 'int'},
      'SURNAME': {type: 'string'},
      'NAME': {type: 'string'},
      'TIN': {type: 'string'},
      'EMAIL': {type: 'string'}
    }
  });
  var Collection = app.classes.collection.extend({
    model: Model,
    mk_serverClass: 'CLIENT'
  });
  return {
    Model: Model,
    Collection: Collection
  };
});