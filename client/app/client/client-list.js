/**
 * Client List Handler
 * @param {type} schema
 * @param {type} cfg
 * @returns {String}
 */
define([
  'appPath/client/client-schema',
  'appPath/client/client-list-cfg'
], function(schema, cfg) {
  var ClientListView = {
    schema: schema,
    view: cfg,
    wantsInsert:false,
    wantsEdit:false,
    wantsDelete:false,
    initialize: function() {
      this.title = 'Clients list';
      this.collection = new schema.Collection();
      this.uber('initialize', arguments);
    },
    setupData: function() {
      this.uber('setupData', arguments);
    }
  };
  return app.classes.ListHandler.extend(ClientListView);
});