define(function() {
  
  /**
   * Users Model
   * @type @exp;Ext@call;define
   */
  var UserModel = Ext.define(null, {
    extend: app.classes.model,
    idAttribute: 'ID',
    mk_serverClass: 'USER',
    mk_fields: {
      'ID': {type: 'int'},
      'TENANTID': {type: 'int'},
      'NAME': {type: 'string'},
      'SURNAME': {type: 'string'},
      'EMAIL': {type: 'string'},
      'CREATED_AT': {type: 'date'}
    }
  });
  
  /**
   * Users Collection
   * @type @exp;Ext@call;define
   */
  var UserCollection = Ext.define(null, {
    extend: app.classes.collection,
    model: UserModel,
    mk_serverClass: 'USER'
  });
  
  /**
   * Return model/collection as modules
   */
  return {
    User: UserModel,
    Users: UserCollection
  };
});