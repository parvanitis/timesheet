define(function () {

    //detail model -> Project
    var ProjectTaskModel = Ext.define(null, {
        extend: app.classes.model,
        idAttribute: 'ID',
        mk_serverClass: 'PROJECTTASK',
        mk_fields: {
            'ID': {type: 'int'},
            'PROJECTID': {type: 'int'},
            'TASKTYPEID': {type: 'int'},
            'USERID': {type: 'int'},
            'NAME': {type: 'string', sortable: true},
            'SURNAME': {type: 'string', sortable: true},
            'TASKDESCRIPTION': {type: 'string', sortable: true},
            'PROJECTNAME': {type: 'string', sortable: true}
        },
        statics: {
            master: 'Project' //setup master -> Project model
        }
    });

    var ProjectTasksCollection = Ext.define(null, {
        extend: app.classes.collection,
        model: ProjectTaskModel,
        mk_serverClass: 'PROJECTTASK',
        byUser: function (uid) {
            var filtered = function (model) {
                return model.get('USERID') == uid;
            };
            return new Collection(filtered);
        }
    });

    //master model
    var ProjectModel = Ext.define(null, {
        extend: app.classes.model,
        idAttribute: 'ID',
        mk_serverClass: 'PROJECT',
        mk_fields: {
            'ID': {type: 'int'},
            'NAME': {label: 'Name', type: 'string', sortable: true},
            'DESCRIPTION': {type: 'string'},
            'START_ON': {type: 'date', defaultValue: Date.now(), validate: 'myValidate'},
            'STATUS': {type: 'int'},
            'ACTIVE': {type: 'int', defaultValue: 1},
            'UIACTIVE': {type: 'string'},
            'ASSIGNEDUSERS': {label: 'Assigned Users', type: 'int', sortable: true},
            'PROJECTTASKS': ProjectTasksCollection //detail collection -> ProjectTasks collection
        },
        myValidate: function (attrs, options) {
            if (!attrs.START_ON || _.isNull(attrs.START_ON))
                return {label: 'Validation error:', msg: 'Start date is invalid'};
        },
        validateRecord: function (attrs, options) {
            if (!attrs.NAME)
                return {label: 'Validation error:', msg: 'Project name is missing'};
        }
    });

    var ProjectsCollection = Ext.define(null, {
        extend: app.classes.collection,
        model: ProjectModel,
        mk_serverClass: 'PROJECT',
        mk_remoteSort: true,
        mk_sort: 'NAME',
        mk_pageSize: (app.layout === 'mobile') ? 10 : 20
//        mk_defaultParams: {where: [{field: 'ACTIVE',op: '=',value: 1}]},
    });

    /**
     * Return collections and models as a named modules
     */
    return {
        Project: ProjectModel,
        Projects: ProjectsCollection,
        ProjectTask: ProjectTaskModel,
        ProjectTasks: ProjectTasksCollection
    };
});