define(function() {
  return function() {
    this.root = {
      items: [
        {
          xtype: 'slickgrid',
          id: 'project-grid',
          collection: this.collection,
          multiSelect: true,
          editable: true,
          autoEdit: true,
          toolbar:false,
          events: {},
          options:{},
          css: {},
          columns: [
            {id: 'NAME', name: 'Name', field: 'NAME', formatter: 'link'},
            {id: 'DESCRIPTION', name: 'Description', field: 'DESCRIPTION'},
            {id: 'USERS', name: 'Users assigned', field: 'ASSIGNEDUSERS'},
            {id: 'UIACTIVE', name: 'ACTIVE', field: 'UIACTIVE',_formatter:this.format_grid}
          ]
        }
      ]
    };
    this.popupSort = {
      id: 'popupSort',
      dismissible: true,
      xtype: 'popup',
      events: {
        close: function(e, popup) {}
      },
      items: [
        {
          html: 'processing, if you close or click it will abort...'
        },
        {
          id: 'progress1',
          html: 'wait...'
        }
      ]
    },
    this.ctxMenu = {
      items: [
        {id: 'action-switch', itemId: 'layout:switch', text: 'Switch'},
        {id: 'action-add', itemId: 'layout:addmodel', text: 'Add model'},
        {id: 'action-back', text: 'Back'}
      ]
    };
  };
});