define(function() {
  return function() {
    this.root = {
      model: this.model,
      items: [
        {
          xtype: 'grid',
          className: 'project-detail',
          columns: 2,
          items: [
            {
              xtype: 'fieldcontain', defaultType: 'textfield',
              items: [
                {label: 'Project Name', fieldName: 'NAME', id: 'NAME'},
                {label: 'Description', fieldName: 'DESCRIPTION', id: 'DESCRIPTION', xtype: 'textarea'},
                {label: 'Started', fieldName: 'START_ON', id: 'START_ON', xtype: 'date'},
                {
                  xtype: 'group',
                  label: 'Active',
                  items: [{
                      xtype: 'flip',
                      fieldName: 'ACTIVE',
                      id: 'ACTIVE'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          xtype: 'spacer'
        },
        {
          xtype: 'spacer'
        },
        {
          xtype: 'grid',
          columns: 1,
          items: [
            {
              xtype: 'fieldcontain',
              items: [
                {
                  xtype: 'slickgrid',
                  collection: 'PROJECTTASKS', //or this.model.get('PROJECTTASKS') :)
                  multiSelect: true,
                  behaviors: [{
                      xtype: 'list',
                      title: 'Associated Tasks and Users',
                      cls: 'appPath/task/task'
                    }],
                  columns: [
                    {id: 'TASKDESCRIPTION', name: 'Task Name', field: 'TASKDESCRIPTION'},
                    {id: 'SURNAME', name: 'Surname', field: 'SURNAME'},
                    {id: 'NAME', name: 'Name', field: 'NAME'}
                  ]
                }
              ]
            }
          ]
        }
      ]
    };
  };
});