define([
    'appPath/project/project-schema',
    'appPath/project/project-cfg'
], function (schema, cfg) {
    var ProjectItemView = {
        schema: schema,
        view: cfg,
        wantsValidate: true,
        wantsRelations: true,
        autoBack: true,
        saveAlways: true,
        events: {
            'click .btn-new': 'do_new',
            'click .btn-edit': 'do_edit',
            'click .btn-taskNew': 'do_addTask'
        },
        listeners: {
            'formhandler:data_stable': 'on_datastable',
            'formhandler:before_save': 'on_beforesave'
        },
        initialize: function () {
            this.title = (this.params.id) ? 'Edit project ' + this.params.id : 'New Project';
            this.schema.model = schema.Project;
            this.uber('initialize', arguments);
        },
        setupData: function () {
            this.uber('setupData', arguments);
        },
        on_datastable: function () {},
        on_beforesave: function () {},
        do_save: function () {
            this.uber('do_save', arguments);
        },
        showErrors: function () {
            this.uber('showErrors', arguments);
        },
        destructor: function () {
            this.uber('destructor', arguments);
        }
    };
    return app.classes.FormHandler.extend(ProjectItemView);
});