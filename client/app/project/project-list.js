/**
 * Project List Handler
 * @param {type} schema
 * @param {type} cfg
 * @returns {String}
 */
define([
    'appPath/project/project-schema',
    'appPath/project/project-list-cfg'
], function (schema, cfg) {
    var ProjectListView = {
        singleton: false, //the handler is not destroyed when navigating away,but hidden, and re-creating the handler always gives the same instance
        schema: schema,
        view: cfg,
        events: {
            'layout:switch': 'do_switch'
        },
        formHandler: {cls: 'appPath/project/project'},
        initialize: function () {
            this.title = 'Projects list';
            this.collection = new schema.Projects();
            this.listenTo(this.collection, 'sync', this.afterFetch);
            this.listenTo(this.collection, 'add', this.onCollectionAdd);
            this.uber('initialize', arguments);
        },
        afterFetch: function () {
            //catch collection 'sync' event
        },
        onCollectionAdd: function () {
            //catch collection 'sync' event
        },
        setupData: function () {
            this.uber('setupData', arguments);
        }
    };
    return app.classes.ListHandler.extend(ProjectListView);
});