define(function() {
  return function() {
    this.root = {
      items: [
        {
          xtype: 'slickgrid',
          id: 'task-grid',
          collection: this.collection,
          multiSelect: true,
          editable: true,
          autoEdit: true,
          events: {},
          columns: [
            {id: 'DESCRIPTION', name: 'Περιγραφή', field: 'DESCRIPTION', formatter: 'link'}
          ]
        }
      ]
    };
  };
});