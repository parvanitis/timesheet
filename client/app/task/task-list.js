/**
 * Task List Handler
 * @param {type} schema
 * @param {type} cfg
 * @returns {String}
 */
define([
  'appPath/task/task-schema',
  'appPath/task/task-list-cfg'
], function(schema, cfg) {
  var TaskListView = {
    schema: schema,
    view: cfg,
    wantsExport: false,
    events: {},
    formHandler: {cls: 'appPath/task/task'},
    initialize: function() {
      this.title = 'Tasks list';
      this.schema.model = schema.Model;
      this.schema.collection = schema.Collection;
      this.uber('initialize', arguments);
    },
    setupData: function() {
      this.uber('setupData', arguments);
    }
  };
  return app.classes.ListHandler.extend(TaskListView);
});