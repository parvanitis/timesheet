define([
  'appPath/project/project-schema',
  'appPath/task/task-schema',
  'appPath/task/task-cfg'
], function(schema, taskSchema, cfg) {
  var TaskItemView = {
    schema: schema,
    view: cfg,
    autoBack: true,
    listeners: {
      'formhandler:data_stable': 'on_datastable',
      'formhandler:before_save': 'on_beforeSave'
    },
    initialize: function() {
      this.title = 'Add new task';
      console.log(app.users);
      this.schema.model = this.schema.ProjectTask;
      this.tasks = new taskSchema.Collection();
      this.tasks.fetch({
        success: _.bind(function() {
          this.uber('initialize', arguments);
        }, this)
      });
    },
    setupData: function() {
      this.uber('setupData', arguments);
    },
    on_datastable: function() {
      lib.log('on_datastable called');
    },
    do_save: function() {
      this.uber('do_save', arguments);
    },
    on_beforeSave: function(ret) {
      lib.log('on_beforeSave called');
    }
  };
  return app.classes.FormHandler.extend(TaskItemView);
});