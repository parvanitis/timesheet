define(function() {
  return function() {
    this.root = {
      model: this.model,
      items: [
        {
          xtype: 'grid',
          columns: 2,
          items: [
            {
              xtype: 'fieldcontain',
              items: [
                {
                  xtype: 'select',
                  collection: this.tasks,
                  label: 'Choose Task',
                  fieldName: 'TASKTYPEID',
                  addEmpty: true,
                  items: [{
                      label: '<%= DESCRIPTION %>',
                      value: '<%= ID %>'
                    }]
                },
                {
                  xtype: 'select',
                  collection: app.users,
                  label: 'Choose User',
                  fieldName: 'USERID',
                  addEmpty: true,
                  items: [{
                      label: '<%= SURNAME %> - <%= NAME %>',
                      value: '<%= ID %>'
                    }]
                }
              ]
            }
          ]
        }
      ]
    };
  };
});