define(function() {
  'use strict';
  var Model = app.classes.model.extend({
    idAttribute: 'ID',
    mk_serverClass: 'TASKTYPE',
    mk_fields: {
      'ID': {type: 'int'},
      'DESCRIPTION': {type: 'string'}
    }
  });
  var Collection = app.classes.collection.extend({
    model: Model,
    mk_serverClass: 'TASKTYPE'
  });
  return {
    Model: Model,
    Collection: Collection
  };
});