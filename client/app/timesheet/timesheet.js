define([
    'appPath/timesheet/timesheet-schema',
    'appPath/timesheet/timesheet-cfg'
], function (schema, cfg) {
    var TimesheetItemView = {
        schema: schema,
        view: cfg,
        wantsValidate: true,
        wantsRelations: true,
        autoBack: true,
        saveAlways: true,
        events: {},
        listeners: {
      'formhandler:data_stable': 'on_datastable',
//      'formhandler:before_save': 'on_beforesave'
        },
        initialize: function () {
            this.title = 'Add new record';
            this.schema.model = schema.Model;
            this.uber('initialize', arguments);
        },
        setupData: function () {
            this.uber('setupData', arguments);
        },
        on_datastable: function () {
//            console.log(lib.moment(this.params.startdate).format('DD/MM/YYYY HH:mm:ss'));x
            this.root.find('STARTDATE').setValue(lib.moment(this.params.startdate).format('DD/MM/YYYY HH:mm:ss'));
            this.root.find('ENDDATE').setValue(lib.moment(this.params.enddate).format('DD/MM/YYYY HH:mm:ss'));
        },
        on_beforesave: function () {
            console.dir(this.model.toJSON());
        },
        do_save: function () {
            this.uber('do_save', arguments);
        },
        showErrors: function () {
            this.uber('showErrors', arguments);
        },
        destructor: function () {
            this.uber('destructor', arguments);
        }
    };
    return app.classes.FormHandler.extend(TimesheetItemView);
});