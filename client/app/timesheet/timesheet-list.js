/**
 * Timesheet List Handler
 * @param {type} schema
 * @param {type} cfg
 * @returns {String}
 */
define([
    'appPath/timesheet/timesheet-schema',
    'appPath/timesheet/timesheet-list-cfg'
], function (schema, cfg) {
    var TimesheetListView = {
        schema: schema,
        view: cfg,
        wantsSearch: false,
        wantsInsert: false,
        wantsEdit: false,
        wantsDelete: false,
        events: {
            'layout:send': 'do_send'
        },
        formHandler: {cls: 'appPath/timesheet/timesheet'},
        initialize: function () {
            this.title = 'Timesheets list';
            this.collection = new schema.Collection();
            this.uber('initialize', arguments);
        },
        do_send: function () {

        },
        afterFetch: function () {
            //catch collection 'sync' event
        },
        onCollectionAdd: function () {
            //catch collection 'sync' event
        },
        setupData: function () {
            this.uber('setupData', arguments);
        }
    };
    return app.classes.ListHandler.extend(TimesheetListView);
});