define(function () {
    'use strict';
    var Model = app.classes.model.extend({
        idAttribute: 'ID',
        mk_serverClass: 'TIMESHEET',
        mk_fields: {
            'ID': {type: 'int'},
            'TENANTID': {type: 'int'},
            'PROJECTID': {type: 'int'},
            'CLIENTID': {type: 'int'},
            'TASKTYPEID': {type: 'int'},
            'PROJECTNAME': {label: 'Project', type: 'string', sortable: true},
            'TASKDESCRIPTION': {label: 'Task', type: 'string', sortable: true},
            'USERNAME': {label: 'Username', type: 'string', sortable: true},
//            'TYPEDESCRIPTION': {label: 'Type', type: 'string'},
            'CLIENTSURNAME': {label: 'Client', type: 'string', sortable: true},
            'DURATION': {label: 'Duration', type: 'varchar', sortable: true},
            'TDATE': {label: 'Date', type: 'date', defaultValue: Date.now(), validate: lib.emptyFn, sortable: true},
            'STARTDATE': {label: 'Date', type: 'string'},
            'ENDDATE': {label: 'Date', type: 'string'},
            'DRAFT': {type: 'int'},
            'ACTIVE': {type: 'int', defaultValue: 1},
            'NOTES': {type: 'string'},
//            'PROJECTNAME': {
//                type: 'string',
//                control: {
//                    xtype: 'lookup',
//                    source: ProjectModel.collection,
//                    dest: {
//                        'PROJECTID': 'ID',
//                        'PROJECTNAME': 'NAME'
//                    },
//                    lineTpl: '<%= ID+" "+NAME %>',
//                    query: {
//                        where: {
//                            field: 'NAME',
//                            op: 'like',
//                            value: '<%=query%>%'
//                        },
//                        extra: 1
//                    }
//                }
//            }
        }
    });
    var Collection = app.classes.collection.extend({
        model: Model,
        mk_serverClass: 'TIMESHEET',
        mk_remoteSort: true,
        mk_sort: 'PROJECTID',
        mk_pageSize: (app.layout === 'mobile') ? 10 : 20,
        mk_defaultParams: {
            where: [{
                    field: 'DRAFT',
                    op: '=',
                    value: 1
                }]
        },
        sync: function () {
            return app.classes.collection.prototype.sync.apply(this, arguments);
        }
    });
    return {
        Model: Model,
        Collection: Collection
    };
});