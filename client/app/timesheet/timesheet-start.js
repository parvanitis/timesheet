define([
    'appPath/timesheet/timesheet-schema',
    'appPath/timesheet/timesheet-start-cfg'
], function (tsSchema, fcfg) {
    var Handler = {
        events: {
            'timer:stop': 'add'
        },
        initialize: function () {
            this.title = 'Home';
            this.model = new tsSchema.Model();
            this.cfg = fcfg.call(this);
            this.root = this.cfg.root;
            this.ready();
        },
        add: function (evt, params) {
            // Find the duration between two dates
            var ts = lib.moment(params.timeStart, 'HH:mm:ss');
            var te = lib.moment(params.timeEnd, 'HH:mm:ss');
            var duration = lib.moment.duration(te - ts);
            var url = {
                cls: 'appPath/timesheet/timesheet',
                params: {
                    duration: duration,
                    startdate: ts,
                    enddate: te
                },
                parent: this
            };
            app.loadContent(url);
        }
    };
    return app.classes.ContentHandler.extend(Handler);
});