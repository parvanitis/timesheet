define(function () {
    return function () {
        this.root = {
            items: [
                {
                    xtype: 'slickgrid',
                    id: 'timesheet-grid',
                    collection: this.collection,
                    multiSelect: false,
                    toolbar: false,
                    events: {},
                    options: {},
                    css: {},
                    columns: [
                        {id: 'PROJECTNAME', name: 'Project', field: 'PROJECTNAME'},
                        {id: 'CLIENTSURNAME', name: 'Client', field: 'CLIENTSURNAME'},
                        {id: 'USERNAME', name: 'User', field: 'USERNAME'},
                        {id: 'TASKDESCRIPTION', name: 'Task', field: 'TASKDESCRIPTION'},
                        {id: 'DURATION', name: 'Duration', field: 'DURATION'}
                    ]
                }
            ]
        };
        this.ctxMenu = {
            items: [
                {id: 'action-send', itemId: 'layout:send', text: 'Send'}
            ]
        };
    };
});