define(function () {
    return function () {
        var cfg = {
            root: {
                items: [
                    {
                        xtype: 'grid',
                        columns: 2,
                        items: [
                            {
                                xtype: 'fieldcontain',
                                items: [
                                    {
                                        xtype: 'countdown'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        };
        return cfg;
    };
});