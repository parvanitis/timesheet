define(function () {
    return function () {
        this.root = {
            model: this.model,
            items: [
                {
                    xtype: 'grid',
                    className: 'timesheet',
                    columns: 2,
                    items: [
                        {
                            xtype: 'fieldcontain',
                            items: [
//                                {
//                                    label: 'name local',
//                                    fieldName: 'NAME',
//                                    className: 'cust-lookup',
//                                    xtype: 'lookup',
//                                    source: this.cust_lookup_2,
//                                    local: true,
//                                    allowEmpty: true,
//                                    autoSelect: false,
//                                    position: false,
//                                    query: function (v) {
//                                        var r = new RegExp('^' + v, 'i');
//                                        return r.test(this.attributes.DESCR);
//                                    }
//                                },
//                                {
//                                    label: 'Project', 
//                                    fieldName: 'PROJECTNAME', 
//                                    xtype: 'lookup', 
//                                    handler: {cls: 'listhandler'}, 
//                                    allowEmpty: true, 
//                                    dest: {custId: 'ID', custDescr: 'DESCR'}
//                                },
                                {
                                    label: 'Project',
                                    fieldName: 'PROJECTNAME',
                                    id: 'PROJECTNAME',
                                    xtype: 'lookup',
                                    handler: {cls: 'appPath/project/project-list'},
                                    allowEmpty: true,
                                    dest: {'PROJECTID': 'ID', 'PROJECTNAME': 'NAME'},
                                    lineTpl: '<%= NAME %>',
//                                    query: {where: {field: 'NAME', op: 'like', value: '%<%=query%>%'}}
                                },
                                {
                                    label: 'Task',
                                    fieldName: 'TASKDESCRIPTION',
                                    id: 'TASKDESCRIPTION',
                                    xtype: 'lookup',
                                    handler: {cls: 'appPath/task/task-list'},
                                    allowEmpty: true,
                                    dest: {'TASKTYPEID': 'ID', 'TASKDESCRIPTION': 'DESCRIPTION'},
                                    lineTpl: '<%= DESCRIPTION %>',
//                                    query: {where: {field: 'DESCRIPTION', op: 'like', value: '%<%=query%>%'}}
                                },
                                {
                                    label: 'Client',
                                    fieldName: 'CLIENTSURNAME',
                                    id: 'CLIENTSURNAME',
                                    xtype: 'lookup',
                                    handler: {cls: 'appPath/client/client-list'},
                                    allowEmpty: true,
                                    dest: {'CLIENTID': 'ID', 'CLIENTSURNAME': 'SURNAME'},
                                    lineTpl: '<%= SURNAME %>',
//                                    query: {where: {field: 'SURNAME', op: 'like', value: '%<%=query%>%'}}
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Start date',
                                    id: 'STARTDATE',
                                    enabled: false,
                                    fieldName: 'STARTDATE',
                                    css: {
                                        'color': '#000',
                                        'font-weight': 'bold'
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'End date',
                                    id: 'ENDDATE',
                                    enabled: false,
                                    fieldName: 'ENDDATE',
                                    css: {
                                        'color': '#000',
                                        'font-weight': 'bold'
                                    }
                                },
                                {
                                    xtype: 'textarea',
                                    label: 'Notes',
                                    columns: 4,
                                    fieldName: 'NOTES'
                                }
                            ]
                        },
                        {
                            type: 'grid',
                            columns: 2,
                            title: 'test',
                            label: 'test2',
                            items: [
                                {
                                    className: 'clock',
                                    items: [
                                        {
                                            className: 'display',
                                            html: this.params.duration.hours() + ' Hours'
                                        }
                                    ]

                                },
                                {
                                    className: 'clock',
                                    items: [
                                        {
                                            className: 'display',
                                            html: this.params.duration.minutes() + ' Minutes'
                                        }
                                    ]

                                },
//                                {
//                                    className: 'clock',
//                                    items: [
//                                        {
//                                            className: 'display',
//                                            html: this.params.duration.seconds() + ' Seconds'
//                                        }
//                                    ]
//
//                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'grid',
                    columns: 2,
                    items: [
                    ]
                },
                {
                    xtype: 'spacer'
                },
                {
                    xtype: 'spacer'
                }

            ]
        };
    };
});