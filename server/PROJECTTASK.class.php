<?php

class PROJECTTASK extends CRUD {

    function __construct() {
        global $db, $config;
        $this->_table = 'project_task';
        $this->_view = "SELECT "
                . "prjt.*,"
                . "ttp.ID as TASKTYPEID,"
                . "ttp.DESCRIPTION as TASKDESCRIPTION,"
                . "usr.NAME as NAME,"
                . "usr.SURNAME as SURNAME,"
                . "usr.ID as PUSERID,"
                . "prj.NAME as PROJECTNAME "
                . "FROM project_task prjt "
                . "JOIN project prj ON prj.ID = prjt.PROJECTID "
                . "JOIN task_type ttp ON ttp.ID = prjt.TASKTYPEID "
                . "JOIN user usr ON usr.ID = prjt.USERID "
                . "/*@w*/";
        $this->_fields = array(
            'ID' => array('type' => 'int', 'origin' => 'prjt.ID'),
            'TENANTID' => array('type' => 'int', 'origin' => 'prjt.TENANTID'),
            'PROJECTID' => array('type' => 'int', 'origin' => 'prjt.PROJECTID'),
            'TASKTYPEID' => array('type' => 'int', 'origin' => 'prjt.TASKTYPEID'),
            'USERID' => array('type' => 'int', 'origin' => 'prjt.USERID'));
        parent::__construct();
    }
    
    function beforeInsertUpdate($isInsert) {
       global $config, $db, $lib;
        $sql = "SELECT count(id) RESULT FROM $config->mainDB.project_task prjt ". 
               "WHERE prjt.PROJECTID=:PROJECTID "
                . "AND prjt.TASKTYPEID=:TASKTYPEID "
                . "AND prjt.USERID=:USERID /*@wa*/";
        $res = $db->fetchFirst($sql, array(
            'PROJECTID' => $this->PROJECTID,
            'TASKTYPEID' => $this->TASKTYPEID,
            'USERID' => $this->USERID
        ));
        $result = pick_value($res, 'RESULT');
        if($result) {
            throw new USER_EXCEPTION('Identical record exists.');    
        }
        parent::beforeInsertUpdate($isInsert);
    }
}
