<?php

class CLIENT extends CRUD {

    function __construct() {
        global $db, $config;
        $this->_table = 'client';
        $this->_descr = 'SURNAME'; //for use in search
        $this->_view = "SELECT * FROM client cl /*@w*/ ORDER BY cl.SURNAME";
        $this->_fields = array(
            'ID' => array('type' => 'int', 'origin' => 'cl.ID'),
            'NAME' => array('type' => 'varchar', 'origin' => 'cl.NAME'),
            'SURNAME' => array('type' => 'varchar', 'origin' => 'cl.SURNAME'),
            'TIN' => array('type' => 'varchar', 'origin' => 'cl.TIN')
        );
        parent::__construct();
    }
}
