<?php

class USER extends CRUD {

    function __construct() {
        global $db, $config;
        $this->_table = 'user';
        $this->_view = "SELECT * FROM user usr";
        $this->_fields = array(
            'ID' => array('type' => 'int', 'origin' => 'usr.ID'),
            'TENANTID' => array('type' => 'int', 'origin' => 'usr.TENANTID'),
            'SURNAME' => array('type' => 'varchar', 'origin' => 'usr.SURNAME'),
            'NAME' => array('type' => 'varchar', 'origin' => 'usr.NAME'),
            'EMAIL' => array('type' => 'varchar', 'origin' => 'usr.EMAIL'),
            'CREATED_AT' => array('type' => 'date', 'origin' => 'usr.CREATED_AT')
        );
        parent::__construct();
    }

}
