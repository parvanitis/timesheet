<?php

class TIMESHEET extends CRUD {

    function __construct() {
        $this->_table = 'timesheet';
        $this->_view = "SELECT ts.ID as TSID,"
                . "ts.TDATE, ts.DURATION, ts.NOTES,"
                . "prj.NAME as PROJECTNAME, usr.SURNAME as USERNAME,"
                . "tt.DESCRIPTION as TASKDESCRIPTION,cl.SURNAME as CLIENTSURNAME "
                . "FROM timesheet ts "
                . "JOIN project prj ON prj.ID = ts.PROJECTID "
                . "JOIN client cl ON cl.ID = ts.CLIENTID "
                . "JOIN task_type tt ON tt.ID = ts.TASKTYPEID "
                . "JOIN user usr ON usr.ID = ts.USERID /*@w*/";
        $this->_fields = array(
            'ID' => array('type' => 'int', 'origin' => 'ts.ID'),
            'TENANTID' => array('type' => 'int', 'origin' => 'ts.TENANTID', 'default' => 1),
            'PROJECTID' => array('type' => 'int', 'origin' => 'ts.PROJECTID'),
            'TASKTYPEID' => array('type' => 'int', 'origin' => 'ts.TASKTYPEID'),
            'CLIENTID' => array('type' => 'int', 'origin' => 'ts.CLIENTID'),
            'USERID' => array('type' => 'int', 'origin' => 'ts.USERID'),
            'DRAFT' => array('type' => 'int', 'default' => 1),
            'DURATION' => array('type' => 'int'),
            'TDATE' => array('type' => 'date'),
            'NOTES' => array('type' => 'varchar', 'view' => true),
            'USERNAME' => array('type' => 'varchar', 'view' => true),
            'PROJECTNAME' => array('type' => 'varchar', 'view' => true),
            'CLIENTNAME' => array('type' => 'varchar', 'view' => true),
            'TASKDESCRIPTION' => array('type' => 'varchar', 'view' => true)
        );
        parent::__construct();
    }

}
