<?php

class PROJECT extends CRUD {

    function __construct() {
        global $db, $config;
        $this->_table = 'project'; //the table to use
        $this->_descr = 'NAME'; //for use in search
        $this->_view = "SELECT " //the class view
                . " prj.ID,prj.NAME,"
                . " prj.DESCRIPTION,"
                . " prj.START_ON,"
                . " prj.ACTIVE,"
                . " COUNT(prjt.ID) as ASSIGNEDUSERS, "
                . " if (prj.ACTIVE=1,'Yes','No') UIACTIVE"
                . " FROM project prj"
                . " LEFT JOIN project_task prjt ON prjt.PROJECTID = prj.ID "
                . " LEFT JOIN task_type ttp ON ttp.ID = prjt.TASKTYPEID "
                . " /*@w*/ GROUP BY prjt.PROJECTID";
        $this->_fields = array(
            'ID' => array('type' => 'int', 'origin' => 'prj.ID'),
            'TENANTID' => array('type' => 'int', 'origin' => 'prj.TENANTID', 'default' => 1),
            'NAME' => array('type' => 'varchar', 'origin' => 'prj.NAME'),
            'DESCRIPTION' => array('type' => 'varchar', 'origin' => 'prj.DESCRIPTION'),
            'START_ON' => array('type' => 'date', 'origin' => 'prj.START_ON'),
            'STATUS' => array('type' => 'int', 'origin' => 'prj.STATUS'),
            'ACTIVE' => array('type' => 'int', 'origin' => 'prj.ACTIVE'),
            'UIACTIVE' => array('type' => 'int', 'origin' => 'prj.UIACTIVE', 'view' => true),
            'ASSIGNEDUSERS' => array('type' => 'int', 'view' => true),
            'PROJECTTASKS' => array(
                'type' => 'relation',
                'relation' => array('class' => 'PROJECTTASK', 'field' => 'PROJECTID')
            )
        );
        parent::__construct();
    }

    protected function beforeInsertUpdate($isInsert) {
        parent::beforeInsertUpdate($isInsert);
    }

}
