<?php

class TASKTYPE extends CRUD {

    function __construct() {
        global $db, $config;
        $this->_table = 'task_type';
        $this->_view = "SELECT * FROM task_type tktp";
        $this->_fields = array(
            'ID' => array('type' => 'int', 'origin' => 'tktp.ID'),
            'DESCRIPTION' => array('type' => 'varchar', 'origin' => 'tktp.DESCRIPTION')
        );
        parent::__construct();
    }

}
