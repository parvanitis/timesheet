<?php

class APP extends APP_BASE {
  const
        TENANT_ID='TENANTID',
        USER_ID='USERID';

  static function getUserID() {
    $user = APP::getUser();
    return $user['ID'];
  }

  static function getTenantID() {
    $tenant = APP::getTenant();
    return $tenant['ID'];
  }
  
  public static function getFileName($cls,$id,$kind,$name){
    global $config;
    if ($cls=='tmp') return addPathChar(self::getTmpDir()) . $name;
    $tenant=self::getTenant();
    $path=fixPathSeparator($config->filePath) . '/' . $tenant['ID'] . '/';
    if ($cls) $path.=$cls.'/';
    if ($id) $path.=$id . '-';
    if ($kind) $path.=$kind . '-';
    if ($name) $path.=$name;
    return mb_strtolower($path);
  }

  static function formatDate($date, $datetime=true) {
    //replace slashes with dots because
    //if the separator is a slash (/), then the American m/d/y is assumed on strtotime.
    //If the separator is a dash (-) or a dot (.), then the European d-m-y format is assumed.
    // works with mysql format
    $date = str_replace('/', '-', $date);
    if($datetime)
      return date("Y-m-d H:i:s", strtotime($date));
    else
      return date("Y-m-d", strtotime($date));
  }

  static function to_timestamp($date) {
    return strtotime(self::formatDate($date, false));
  }

  static function startsWith($haystack, $needle) {
    return !strncmp($haystack, $needle, strlen($needle));
  }

  static function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
  }

}